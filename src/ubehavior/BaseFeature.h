#pragma once

#include "StepInfo.h"

#include <deque>

namespace ubehavior{

struct BaseFeature{
    virtual ~BaseFeature(){
        finish();
    }

    void beforeStep(const char * keyword){
        if(hasRunningStep()){//inner step
            return;
        }

        stepStack.push_back({keyword});
    }

    void startStep(const char * stepMethod, char const* file, std::size_t line=0){
        if(hasRunningStep()){ //occur when this is an innerStep
            return;
        }

        if(stepStack.empty() || lastStep().finished()){
            beforeStep("");
        }

        lastStep()
                .setMethodName(stepMethod)
                .setLineInfo(file, line)
                .start();

        onStepStart(lastStep());
    }

    void endStep(){
        if(hasRunningStep()){
            lastStep().finish();
        }
    }

    void failed(){
        failTest(lastStep());
    }

    void finish(){
        clearSteps();
    }

    void clearSteps(){
        while(!stepStack.empty()){
            onStepFinish(lastStep());

            stepStack.pop_back();
        }
    }

protected:
    class ScopedStep{
    public:
        ScopedStep(BaseFeature * feature, bool inner=false)
            : feature(feature)
            , innerStep(inner)
        {
        }
        ~ScopedStep()
        {
            if(!innerStep){
                feature->endStep();
            }
        }

        BaseFeature * feature;
        bool innerStep;
    };

    ScopedStep startScopedStep(const char * stepMethod, char const* file, std::size_t line=0){
        bool isInner = hasRunningStep();

        startStep(stepMethod, file, line);

        return ScopedStep(this, isInner);
    }

    bool hasRunningStep() const{
        return !stepStack.empty() && lastStep().started() && !lastStep().finished();
    }

protected:
    virtual void onStepStart(StepInfo &){}

    virtual void onStepFinish(StepInfo &){}

    virtual void failTest(StepInfo &){}

    StepInfo & lastStep(){
        return stepStack.back();
    }
    const StepInfo & lastStep() const{
        return stepStack.back();
    }

    std::deque<StepInfo> stepStack;
};

}//namespace
