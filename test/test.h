#pragma once

#define CATCH_CONFIG_FAST_COMPILE
#include <catch.hpp>

#define FIXTURE_SCENARIO(msg, ...) \
        SCENARIO_METHOD(FIXTURE, msg, TEST_TAG __VA_ARGS__)

#define SCENARIO_PENDING(msg, ...)\
        TEST_CASE(msg, "[!mayfail]" __VA_ARGS__){ FAIL("not implemented yet!"); }

#define AND( desc ) SECTION( std::string("     And: ") + desc, "" )
