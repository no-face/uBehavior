#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conan.packager import ConanMultiPackager

import os

if __name__ == "__main__":
    builder = ConanMultiPackager()

    # Header-only no options
    builder.add(options={})

    builder.run()
