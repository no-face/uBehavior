#include "uBehaviorScenarios.h"

#define FIXTURE uBehaviorScenarios
#define TEST_TAG "[uBehavior]"

/* *********************************************************/

FIXTURE_SCENARIO("[uBehavior] enables specify a scenario using methods"){
    feature
        .given().some_condition()
        .when().some_action_is_executed()
        .then().a_result_is_expected();

    CHECK(feature.conditionCount    == 1);
    CHECK(feature.actionCount       == 1);
    CHECK(feature.checkCount        == 1);
}
