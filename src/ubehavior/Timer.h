#pragma once

#include <chrono>

namespace ubehavior{

/**
    Class to count elapsed time. Based on: https://gist.github.com/gongzhitaao/7062087
*/
class Timer{
public:
    void start(){
        reset();
    }

    void reset(){
        begin = Clock::now();
    }

    double getElapsedSeconds(){
        return std::chrono::duration_cast<Second>(Clock::now() - begin).count();
    }

protected:
    using Clock = std::chrono::high_resolution_clock;
    using Second = std::chrono::duration<double, std::chrono::seconds::period>;
    std::chrono::time_point<Clock> begin;
};

}//namespace
