#pragma once

#include "test.h"

#include "ubehavior/Feature.h"

using namespace ubehavior;

class FeatureOnTest : public Feature<FeatureOnTest>{
public:
    using super = Feature<FeatureOnTest>;

    using super::stepStack;

    void onStepStart(StepInfo & stepInfo) override{
        this->capturedStepInfo = stepInfo;
    }

    StepInfo capturedStepInfo;
};

class FeatureTests{
public:

public: //actions
    void callKeyword(const std::string & keyword){
        feature.keyword(keyword.c_str());
    }

public: //checks
    void requireFeatureNotStarted(){
        REQUIRE(feature.stepStack.empty());
    }

    void stepKeywordShouldBe(const std::string & expectedKeyword){
        CHECK(getLastStep().getKeyword() == expectedKeyword);
    }

protected:
    StepInfo & getLastStep(){
        REQUIRE_FALSE(feature.stepStack.empty());
        return feature.stepStack.back();
    }


    StepInfo & capturedStepInfo(){
        return feature.capturedStepInfo;
    }

protected:
    FeatureOnTest feature;
};
