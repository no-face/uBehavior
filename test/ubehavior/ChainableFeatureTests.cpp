#include "ChainableFeatureTests.h"

#define FIXTURE FeatureTests
#define TEST_TAG "[Feature]"

/* *********************************************************/

FIXTURE_SCENARIO("[Feature] should initiate steps when calling keywords"){

GIVEN("a non started feature instance"){
    requireFeatureNotStarted();

WHEN("Calling some keyword"){
    callKeyword("myKeyword");

THEN("a new StepInfo should be created with used keyword"){
    stepKeywordShouldBe("myKeyword");

AND("this StepInfo should not have started"){
    CHECK_FALSE(getLastStep().started());
}
}
}
}

}//scenario

FIXTURE_SCENARIO("[Feature] should update last step info when starting step"){
GIVEN("a feature that have a keyword called"){
    callKeyword("given");

WHEN("a feature step is started"){
    feature.startStep("step name", "filename.cpp", 123 /* line */);

THEN("the feature 'onStepStart' method should be called with the started stepInfo"){
    CHECK(capturedStepInfo().started());
    CHECK(capturedStepInfo().getKeyword()     == "given");
    CHECK(capturedStepInfo().getMethodName()  == "step name");
    CHECK(capturedStepInfo().getFilename()    == "filename.cpp");
    CHECK(capturedStepInfo().getLine()        == 123);
}
}
}
}//scenario
