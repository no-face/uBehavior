#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
import os
from os import path

class Recipe(ConanFile):
    # Header only
    #settings = "os", "compiler", "build_type", "arch"

    name        = "uBehavior"
    version     = "0.1.1"
    description = "Library to build gherkin style bdd tests in c++"
    license     = "unlicense (public domain)"

    #Conan dependencies
    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.7@noface/stable"
    )

    test_requirements = (
        "Catch/1.9.4@uilianries/stable",
        "FakeIt/2.0.4@noface/stable",
    )

    generators = "Waf"

    default_options = (
        "FakeIt:header_version=standalone",
    )

    exports_sources="src/*", "test/*", "wscript"

    def build_requirements(self):
        if not self.build_tests:
            return

        for r in self.test_requirements:
            self.build_requires(r)

    def imports(self):
        # Copy waf executable to project folder
        self.copy("waf", dst=".")

    def build(self):
        opts = self.waf_options()

        cmd = 'waf configure build -o build {}'.format(opts)
        self.output.info(cmd)
        self.run(cmd, cwd = self.build_folder)

    def package(self):
        self.run("waf install", cwd=self.build_folder)

    def package_info(self):
        self.cpp_info.libdirs = []
        self.cpp_info.resdirs = []
        self.cpp_info.bindirs = []

    def package_id(self):
        self.info.header_only()

    def waf_options(self):
        opts = []

        if not hasattr(self, "package_folder"):
            self.package_folder = path.abspath(path.join(".", "package"))

        opts.append("--prefix=%s" % self.package_folder)
        opts.append("--build-tests={}".format(self.build_tests))

        return " ".join(opts)

    @property
    def build_tests(self):
        # Build tests only if running in user folder
        # (not in local cache) and is developing

        return not self.in_local_cache and self.develop
