#pragma once

#include "BaseFeature.h"

namespace ubehavior{

template <typename T>
class Feature : public BaseFeature{
public:
    using Self = T;

    ~Feature(){
        finish();
    }

    Self & given(){ return keyword("given");}
    Self & when() { return keyword("when"); }
    Self & then() { return keyword("then"); }
    Self & and_() { return keyword("and"); }
    Self & but()  { return keyword("but"); }

    Self & keyword(const char * word) {
        beforeStep(word);

        return self();
    }

    T & self(){
        return *selfPtr();
    }
    T * selfPtr(){
        return (T *)this;
    }
};

}//namespace
