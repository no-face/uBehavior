#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path

from waflib.Configure import conf

def load_tools(ctx):
    ctx.load('compiler_cxx')
    ctx.load('gnu_dirs')

def options(ctx):
    #Load options from waf tools (c++ compiler)
    # Execute ./waf --help to see current options
    load_tools(ctx)

    #You can add your own options also

    ctx.add_option('--debug', action='store_true', default=True, dest='debug', help='Do debug build')
    ctx.add_option('--release', action='store_false', dest='debug', help='Do release build')

    # Flag to generate shared or static libs
    #ctx.add_option('--shared', action='store_true', default=False, help='Build libs as shared libraries')

    ctx.add_option('--build-tests', action='store', dest='build_tests', default=True, help='Build tests')


def configure(ctx):
    load_tools(ctx)

    # Load references to conan dependencies.
    # This file (conanbuildinfo_waf.py) will be created
    # by the waf generator after running 'conan install'
    ctx.load('conanbuildinfo_waf', tooldir=[".", path.join(ctx.bldnode.abspath(), "..")]);

    # Allows debug build
    if ctx.options.debug:
        ctx.env['CXXFLAGS'] += ['-g']

    ctx.env.CXXFLAGS += ['-std=c++11', '-Wall']

    ctx.env.build_tests    = (ctx.options.build_tests == True or ctx.options.build_tests == 'True')

    ctx.recurse('test')

def build(bld):
    bld.recurse('src')
    bld.recurse('test')

################################### Helpers ###########################################

@conf
def glob(ctx, *k, **kw):
    '''Helper to execute an ant_glob search.
        See documentation at: https://waf.io/apidocs/Node.html?#waflib.Node.Node.ant_glob
    '''

    return ctx.path.ant_glob(*k, **kw)

@conf
def lib(bld, **kw):
    from waflib import Utils, Logs
    from waflib.Tools import c_aliases

    if 'install_path' not in kw:
        kw['install_path'] = bld.env.LIBDIR


    _lib_build_objects(bld, kw)

    _lib_build_library(bld, **kw)

    _lib_install_headers(bld, **kw)

def _lib_build_objects(bld, kw):
    # NOTE: keywords (kw) should be passed as dict to allow
    # changing it

    from waflib import Utils, Logs

    source = kw.get('source', [])

    if not source:
        return

    only_objects = kw.get("only_objects", False)

    objects_kw = kw.copy()

    if not only_objects:
        objects_target = kw.get('target') + ".objects"
        objects_kw['target'] = objects_target

    bld.objects(**objects_kw)

    # Changes 'use' to depends on built objects
    kw['use'] = [objects_target] + Utils.to_list(kw.get('use', ""))

def _lib_build_library(bld, **kw):
    if not kw.get("source", []): #header only library
        bld(**kw)

    elif not kw.get("only_objects", False):
        lib_type = "shlib" if bld.env.shared else "stlib"
        c_aliases.set_features(kw, lib_type)

        # remove source to not build the same sources again
        kw['source']   = []

        # build library
        bld(**kw)

def _lib_install_headers(bld, **kw):
    from waflib import Utils

    includedir = kw.get('install_includedir', bld.env.INCLUDEDIR)

    install_headers = []

    if 'install_headers' in kw:
        install_headers = kw['install_headers']
    else:
        headers_dirs = kw.get('headers_dirs', kw.get('export_includes', []))

        for inc_dir in headers_dirs:

            inc_node = bld.path.make_node(inc_dir)
            incs = inc_node.ant_glob([
                                path.join('**', '*.h'),
                                path.join('**', '*.hpp')])

            install_headers.extend(incs)

    if install_headers:
        headers = Utils.to_list(install_headers)

        headers_base = kw.get("headers_base", None)
        relative_trick = kw.get("install_relative", True)
        bld.install_files(
            includedir,
            headers,
            relative_trick=relative_trick,
            cwd=headers_base)
