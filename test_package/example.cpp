#include "ubehavior/ubehavior.h"

#include <iostream>

class MyFeature : public ubehavior::Feature<MyFeature>{
public:

    STEP_given_(some_condition)()
    {}

    STEP_when_(something_happens)(){}

    STEP_then_(a_result_is_expected)(){}

    void onStepStart(ubehavior::StepInfo & stepInfo) override{
        std::cout << stepInfo.getName() << std::endl;
    }
};

int main(){
    MyFeature feature;

    std::cout << "*************** start uBehavior example ********************" << std::endl;

    feature
        .given().some_condition()
        .when().something_happens()
        .then().a_result_is_expected();

    std::cout << "*************** finish uBehavior example *******************" << std::endl;

    return 0;
}
