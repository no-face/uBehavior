#pragma once

#include "Timer.h"

#include <string>
#include <algorithm>

namespace ubehavior {

class StepInfo{
public:
    StepInfo(const char * keyword="") : keyword(keyword)
    {}

    void start(){
        stepTimer.start();
        this->_started = true;
    }

    void finish(){
        durationInSeconds = stepTimer.getElapsedSeconds();
    }

    bool finished() const{
        return durationInSeconds != 0;
    }

    bool started() const{
        return _started;
    }

public:
    const std::string & getFilename() const{
        return file;
    }
    std::size_t getLine() const{
        return line;
    }
    const std::string & getMethodName() const{
        return methodName;
    }
    const std::string & getKeyword() const{
        return keyword;
    }
    std::string getName() const{
        std::string name(keyword);
        if(!methodName.empty()){
            if(!name.empty()){
                name.append("\t");
            }
            std::string mName {methodName};
            std::replace( mName.begin(), mName.end(), '_', ' ');

            name.append(mName);
        }

        return name;
    }

    double getDurationInSeconds() const{
        return durationInSeconds;
    }

    StepInfo & setLineInfo(const char * file, std::size_t line){
        this->file = file;
        this->line = line;

        return *this;
    }

    StepInfo & setMethodName(const char * methodName){
        this->methodName = methodName;
        return *this;
    }


    StepInfo & setKeyword(const char * keyword){
        this->keyword = keyword;
        return *this;
    }

private:
    std::string keyword, file, methodName;
    std::size_t line = 0;

    Timer stepTimer;
    double durationInSeconds=0.0;
    bool _started = false;
};

}//namespace
