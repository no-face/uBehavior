#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
import os
from os import path

class TestRecipe(ConanFile):
    settings = "os", "compiler", "arch"
    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.7@noface/stable"
    )

    generators = "Waf"
    exports = "wscript"

    def imports(self):
        self.copy("*.dll"   , src="bin", dst="bin")
        self.copy("*.dylib*", src="lib", dst="bin")

    def build(self):
        self.build_path = path.abspath("build")

        self.run(
            "waf configure build -o %s" % (self.build_path),
            cwd=self.conanfile_directory)

    def test(self):
        exec_path = path.join(self.build_path, 'example')
        self.output.info("running test: " + exec_path)
        self.run(exec_path)
