# μBehavior (WIP)

Library for create c++ tests using a BDD like syntax (inspired by [JGiven](http://jgiven.org/)).

> The library name is pronounced as 'micro behavior'. The first letter is the 'micro' symbol
(the greek letter mμ), but is written as 'u', to ease write the name.

## Build

1. Install conan dependencies

        conan install
2. Build

        conan build

   or

        ./waf configure build

## License

The project is released under the 'unlicense' license (see UNLICENSE.md).
That is, the software is put into the public domain.

So, do good things with this!  ( ^_^)b
