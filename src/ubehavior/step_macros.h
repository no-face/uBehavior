#pragma once

#define STEP_DECL_IMPL(file, line, method, impl) \
    template<typename ...Args> \
    Self & method(Args&&... args){\
        auto step = this->startScopedStep(#method, file, line);\
        try{\
            impl(std::forward<Args>(args)...);\
        }\
        catch(...){\
            this->failed();\
        }\
        \
        return this->self();\
    }

#define STEP_DECL(method, impl) \
    STEP_DECL_IMPL(__FILE__, __LINE__, method, impl) \

#define STEP_METHOD_IMPL(method, impl) \
    STEP_DECL(method, impl) \
    void impl

#define STEP_METHOD(method) \
    STEP_METHOD_IMPL(method, method##_impl)

#define STEP_ALIAS(alias, method) \
    STEP_DECL(alias, method##_impl)


/************************ Alias **************************/
#define STEP_given_(method) STEP_METHOD(method)
#define STEP_when_(method)  STEP_METHOD(method)
#define STEP_then_(method)  STEP_METHOD(method)
