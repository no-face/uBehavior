#pragma once

#include "test.h"

#include "ubehavior/catch.h"

using namespace ubehavior;

class SampleFeature : public CatchFeature<SampleFeature>{
public:
    STEP_given_(some_condition)(){
        ++conditionCount;
    }
    STEP_when_(some_action_is_executed)(){
        ++actionCount;
    }
    STEP_then_(a_result_is_expected)(){
        ++checkCount;
    }

public:
    int conditionCount = 0, actionCount =0, checkCount = 0;

};

class uBehaviorScenarios{
public:

    SampleFeature feature;
};
