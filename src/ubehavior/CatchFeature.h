#pragma once

#include "Feature.h"

#include <catch.hpp>

#include <deque>

namespace ubehavior{

template<typename T>
class CatchFeature : public Feature<T>{
public:
    using super = Feature<T>;

    ~CatchFeature(){
        /*Workaround: call finish here, because otherwise the 'CatchFeature::onStepFinish'
            will not be called (this class will be already destroyed)
        */
        super::finish();
    }

    void onStepStart(StepInfo & step) override{
        catchSections.push_back({{step.getFilename().c_str(), step.getLine()}, step.getName()});

        catchSectionIncluded.push_back(Catch::getResultCapture().sectionStarted(catchSections.back(), counts));
    }

    void onStepFinish(StepInfo & step) override
    {
        if(catchSections.empty()){
            return; //TODO: warn error
        }

        if(catchSectionIncluded.back()){

            if(std::uncaught_exception()){
                counts.failed ++;
            }

            Catch::SectionEndInfo endInfo{catchSections.back(), counts, step.getDurationInSeconds()};

            Catch::getResultCapture().sectionEnded(endInfo);
        }

        catchSections.pop_back();
        catchSectionIncluded.pop_back();
    }



    void failTest(StepInfo & currentStep) override{
        auto resultDisposition = Catch::ResultDisposition::Normal;
        Catch::SourceLineInfo lineInfo{currentStep.getFilename().c_str(), currentStep.getLine()};
        Catch::ResultBuilder __catchResult(
                    "Step failed", std::move(lineInfo), "", resultDisposition);
        __catchResult.useActiveException( resultDisposition );
        __catchResult.react();
    }


    std::deque<Catch::SectionInfo> catchSections;
    std::deque<bool> catchSectionIncluded;

    Catch::Counts counts;
};

}//namespace
